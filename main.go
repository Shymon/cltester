package main

import (
	"bufio"
	"errors"
	"fmt"
	"io"
	"io/ioutil"
	"log"
	"os"
	"os/exec"
	"path"
	"strings"

	"github.com/fatih/color"
)

type OutputTypeEnum string

const (
	Exact   OutputTypeEnum = "exact"
	Partial OutputTypeEnum = "partial"

	ProgramFileNamePrefix string = "# Program filename: "

	ScenarioPrefix string = "###"
	CommentPrefix  string = "#"

	InputPrefix     string = "<-"
	InputPrefixSize int    = 3

	OutputSkipPrefix   string = "-|"
	OutputParialPrefix string = "-)"
	OutputExactPrefix  string = "->"

	ScenarioFileName string = "scenario.txt"
)

var ProgramFileName string

type Command string

type Scenario struct {
	Name     string
	Commands []Command
	Success  bool
}

func main() {
	if _, err := os.Stat(ScenarioFileName); os.IsNotExist(err) {
		color.Yellow("Missing scenario.txt file, creating it ...")
		createScenarioFile()
		return
	}

	ProgramFileName = loadProgramFileNameFromFile(ScenarioFileName)
	if ProgramFileName == "" {
		panic(errors.New("invalid program file name"))
	}
	lines := loadLinesFromFile(ScenarioFileName)
	scenarios := composeScenarios(lines)
	executeScenarios(&scenarios)
	printStatus(scenarios)
}

func executeScenarios(scenarios *[]Scenario) {
	color.Blue("Got " + fmt.Sprint(len(*scenarios)) + " scenarios to check --------------------")
	for idx, scenario := range *scenarios {
		inputs := convertCommandsToInput(scenario.Commands)
		programOutput := runProgram(inputs)
		color.Cyan("Scenario: " + scenario.Name)
		success := validateOutput(programOutput, scenario.Commands)
		if success {
			color.Green("Ok")
			(*scenarios)[idx].Success = true
		} else {
			color.Magenta("Failed")
			(*scenarios)[idx].Success = false
		}
		color.Blue("---")
	}
}

func validateOutput(programOutput []string, commands []Command) bool {
	var outputCommands []Command
	for _, command := range commands {
		if strings.HasPrefix(string(command), OutputParialPrefix) ||
			strings.HasPrefix(string(command), OutputExactPrefix) ||
			strings.HasPrefix(string(command), OutputSkipPrefix) {
			outputCommands = append(outputCommands, command)
		}
	}

	for idx, command := range outputCommands {
		if strings.HasPrefix(string(command), OutputSkipPrefix) {
			continue
		}
		if len(programOutput) < idx {
			color.Red("Missing output for " + string(command))
			return false
		}

		if strings.HasPrefix(string(command), OutputParialPrefix) {
			input := strings.TrimSpace(strings.Replace(string(command), OutputParialPrefix, "", 1))
			if strings.Contains(programOutput[idx], input) {
				color.Green("\"" + programOutput[idx] + "\"" + " contains " + "\"" + input + "\"")
			} else {
				color.Red("\"" + programOutput[idx] + "\"" + " does not contain " + "\"" + input + "\"")
				return false
			}
		}

		if strings.HasPrefix(string(command), OutputExactPrefix) {
			input := strings.TrimSpace(strings.Replace(string(command), OutputExactPrefix, "", 1))
			output := strings.TrimSpace(programOutput[idx])
			if output == input {
				color.Green("Got exact: " + "\"" + output + "\"")
			} else {
				color.Red("\"" + output + "\"" + " is not " + "\"" + input + "\"")
				return false
			}
		}
	}
	return true
}

func printStatus(scenarios []Scenario) {
	var successful []Scenario
	for _, scenario := range scenarios {
		if scenario.Success {
			successful = append(successful, scenario)
		}
	}
	if len(successful) == len(scenarios) {
		color.Green("All tests passed (" + fmt.Sprint(len(successful)) + "/" + fmt.Sprint(len(scenarios)) + ")")
	} else {
		color.Red("Some tests failed, only (" + fmt.Sprint(len(successful)) + "/" + fmt.Sprint(len(scenarios)) + ")" + " passed")
	}
}

func convertCommandsToInput(commands []Command) (inputs []string) {
	for _, command := range commands {
		if strings.HasPrefix(string(command), InputPrefix) {
			inputs = append(inputs, string(command)[InputPrefixSize-1:])
		}
	}
	return
}

func runProgram(inputs []string) []string {
	currentDir, err := os.Getwd()
	if nil != err {
		log.Fatalf("Error obtaining current dur: %s", err.Error())
	}
	cmd := exec.Command(path.Join(currentDir, ProgramFileName))
	cmd.Stderr = os.Stderr
	stdin, err := cmd.StdinPipe()
	if nil != err {
		log.Fatalf("Error obtaining stdin: %s", err.Error())
	}
	stdout, err := cmd.StdoutPipe()
	if nil != err {
		log.Fatalf("Error obtaining stdout: %s", err.Error())
	}

	var output []string
	reader := bufio.NewReader(stdout)
	go func(reader io.Reader) {
		scanner := bufio.NewScanner(reader)
		for scanner.Scan() {
			output = append(output, scanner.Text())
		}
	}(reader)

	if err := cmd.Start(); nil != err {
		log.Fatalf("Error starting program: %s, %s", cmd.Path, err.Error())
	}
	for _, input := range inputs {
		stdin.Write([]byte(input + "\n"))
	}
	cmd.Wait()
	return output
}

func composeScenarios(lines []string) (scenarios []Scenario) {
	var scenario Scenario
	for idx, line := range lines {
		if strings.HasPrefix(line, ScenarioPrefix) {
			name := strings.Replace(line, ScenarioPrefix+" ", "", 1)
			if idx != 0 {
				scenarios = append(scenarios, scenario)
				scenario = Scenario{Name: name}
				continue
			}
			scenario = Scenario{Name: name}
			continue
		}

		scenario.Commands = append(scenario.Commands, Command(line))
	}
	scenarios = append(scenarios, scenario)
	return
}

func loadLinesFromFile(filename string) []string {
	data, err := ioutil.ReadFile(filename)
	if err != nil {
		panic(err)
	}
	strData := string(data)
	dirtyLines := strings.Split(strData, "\n")
	var lines []string
	for _, line := range dirtyLines {
		if strings.HasPrefix(line, ScenarioPrefix) {
			lines = append(lines, line)
			continue
		}
		if strings.HasPrefix(line, CommentPrefix) || len(strings.TrimSpace(line)) == 0 {
			continue
		}
		lines = append(lines, line)
	}
	return lines
}

func loadProgramFileNameFromFile(filename string) string {
	data, err := ioutil.ReadFile(filename)
	if err != nil {
		panic(err)
	}
	strData := string(data)
	lines := strings.Split(strData, "\n")
	for _, line := range lines {
		if strings.HasPrefix(line, ProgramFileNamePrefix) {
			return strings.TrimSpace(strings.Replace(line, ProgramFileNamePrefix, "", 1))
		}
	}
	return ""
}

func createScenarioFile() {
	d1 := []byte(`
# Program filename: example_program.sh
# Lines starting with "#" are comments and are omitted 
# Scenario starts with three "#" like "### Scenario one"
# Scenario syntax:
# "<- x y" - input to program
# "-> x y" - expect exact same output
# "-) y" - expect output to contain value
# "-|" - skip output

### Scenario 1
-|
<- 1 2
-> you typed: 1 2
<- 3 4
-> you typed: 3 4

### Scenario 2
-|
<- 1 2
-) 1 2
<- 3 5
-) 3 4

### Scenario 3
-|
<- 1 2
-) 1 2
<- 3 4
-) 3 4
	`)
	err := ioutil.WriteFile("scenario.txt", d1, 0644)
	if err != nil {
		panic(err)
	}
}
